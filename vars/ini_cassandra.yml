---
# Copyright 2015 Hewlett-Packard Development Company, L.P.
# Copyright 2016-2017 FUJITSU LIMITED
# Copyright 2018 SUSE Linux GmbH

# wrapper for monasca-api settings, used by ini_encode
monasca_api_ini_conf_wrapper:
  DEFAULT:
    log_config_append: "{{ monasca_api_conf_logging }}"
    region: "{{ api_region }}"
  dispatcher:
    versions: monasca_api.v2.reference.versions:Versions
    version_2_0: monasca_api.v2.reference.version_2_0:Version2
    metrics: monasca_api.v2.reference.metrics:Metrics
    metrics_measurements: monasca_api.v2.reference.metrics:MetricsMeasurements
    metrics_statistics: monasca_api.v2.reference.metrics:MetricsStatistics
    metrics_names: monasca_api.v2.reference.metrics:MetricsNames
    alarm_definitions: monasca_api.v2.reference.alarm_definitions:AlarmDefinitions
    alarms: monasca_api.v2.reference.alarms:Alarms
    alarms_count: monasca_api.v2.reference.alarms:AlarmsCount
    alarms_state_history: monasca_api.v2.reference.alarms:AlarmsStateHistory
    notification_methods: monasca_api.v2.reference.notifications:Notifications
    dimension_values: monasca_api.v2.reference.metrics:DimensionValues
    dimension_names: monasca_api.v2.reference.metrics:DimensionNames
    notification_method_types: monasca_api.v2.reference.notificationstype:NotificationsType
    healthchecks: monasca_api.healthchecks:HealthChecks
  repositories:
    metrics_driver: monasca_api.common.repositories.cassandra.metrics_repository:MetricsRepository
    alarm_definitions_driver: monasca_api.common.repositories.sqla.alarm_definitions_repository:AlarmDefinitionsRepository
    alarms_driver: monasca_api.common.repositories.sqla.alarms_repository:AlarmsRepository
    notifications_driver: monasca_api.common.repositories.sqla.notifications_repository:NotificationsRepository
    notification_method_type_driver: monasca_api.common.repositories.sqla.notification_method_type_repository:NotificationMethodTypeRepository
  messaging:
    driver: monasca_api.common.messaging.kafka_publisher:KafkaPublisher
  security:
    default_authorized_roles: "{{ default_authorized_roles }}"
    agent_authorized_roles: "{{ agent_authorized_roles }}"
    read_only_authorized_roles: "{{ read_only_authorized_roles }}"
    delegate_authorized_roles: "{{ delegated_authorized_roles }}"
  database:
    connection: "{{ database_driver }}://{{ database_user }}:{{ database_monapi_password }}@{{ database_host }}/{{ database_name }}"
  kafka:
    uri: "{{ kafka_hosts }}"
    max_retry: "{{ monasca_api_kafka_max_retry|int }}"
    wait_time: "{{ monasca_api_kafka_wait_time|int }}"
    metrics_topic: "{{ monasca_api_kafka_metrics_topic }}"
    group: "{{ monasca_api_kafka_group }}"
    async: "{{ monasca_api_kafka_async|bool }}"
    compact: "{{ monasca_api_kafka_compact|bool }}"
    partitions: "{{ monasca_api_kafka_partitions|int }}"
  cassandra:
    cluster_ip_addresses: "{{ cassandra_hosts }}"
    contact_points: "{{ cassandra_hosts }}"
    user: "{{ cassandra_mon_api_role }}"
    password: "{{ cassandra_mon_api_password }}"
  keystone_authtoken:
    auth_type: password
    auth_url: "{{ keystone_protocol }}://{{ keystone_host }}:{{ keystone_port|int }}"
    www_authenticate_uri: "{{ keystone_protocol }}://{{ keystone_host }}:{{ keystone_port|int }}"
    auth_version: v3.0
    region_name: "{{keystone_region}}"
    password: "{{ keystone_admin_password }}"
    username: "{{ keystone_admin }}"
    insecure: "{{ keystone_insecure|bool }}"
    project_name: "{{keystone_admin_project}}"
    project_domain_name: "{{keystone_project_domain_name}}"
    user_domain_name: "{{keystone_user_domain_name}}"
